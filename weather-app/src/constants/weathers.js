/**
 * se crea un archivo de constantes para que los parametros que se pasan
 * al componente en este caso 'WeatherTempeture' sean los correctos
 */

export const CLOUD = "cloud";

export const CLOUDY = "cloudy";

export const SUN = "sun";

export const RAIN = "rain";

export const SNOW = "snow";

export const WINDY = "windy";