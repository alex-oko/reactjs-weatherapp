import React, { Component } from 'react';
import WeatherIcons from 'react-weathericons'; //iconos 
import PropTypes from 'prop-types'; //valida el tipo de dato que voy a usar en el componente

import { 
    CLOUD,
    CLOUDY,
    SUN,
    RAIN,
    SNOW,
    WINDY,
 } from "../../../constants/weathers";

/**
 * Se puede declarar una variable this.icons en el metodo contructor
 * o se puede utilizar una const por fuera de la clase const icons
 * 
 * Ahora el nombre de la llave del objeto es la constante que se importaron
 */
const icons = {
    [CLOUD]: "wi wi-cloud",
    [CLOUDY]: "wi wi-cloudy",
    [SUN]: "wi wi-day-sunny",
    [RAIN]: "wi wi-day-rain",
    [SNOW]: "wi wi-snow",
    [WINDY]: "wi wi-windy"

}

class WheaterTempeture extends Component {
    constructor(props){
        super(props);
        /*this.icons = {
            sunny: "wi wi-day-sunny"
        }*/
    }

    getWeatherIcon(weatherState){
        //console.log(this.icons)
        //console.log(icons);
        const icon = icons[weatherState]
        const sizeIcon = "2x"
        return <WeatherIcons name={icon ? icon : "wi wi-day-sunny"} size={sizeIcon} />
    }

    render() {
        //let temperature:Number = this.props.temperature;
        const { temperature, weatherState} = this.props;
         //<WeatherIcons name="cloud" size={sizeIcon} />
        return (
            <div className="weater-tempeture-content">
                { this.getWeatherIcon(weatherState) }
                <p className="temperature">{`${temperature}º`}</p>
            </div>
        );
    }
}

/**
 * Definimos el tipo de dato que debe llegar para ser usado en el componente
 */
WheaterTempeture.propTypes = {
    temperature: PropTypes.number,
    weatherState: PropTypes.string
}

export default WheaterTempeture;