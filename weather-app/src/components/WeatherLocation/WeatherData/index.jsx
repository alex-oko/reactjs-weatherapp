import React, { Component } from 'react';
import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTempeture from './WheaterTempeture';

import { 
    CLOUD,
    CLOUDY,
    SUN,
    RAIN,
    SNOW,
    WINDY,
 } from "../../../constants/weathers";

 import './styles.css';

class WeatherData extends Component {
    render() {
        return (
            <div className="weather-data-content">
                <WeatherTempeture temperature={20} weatherState={RAIN}/>
                <WeatherExtraInfo humidity={80} wind={"10 m/s"} />
            </div>
        )
    }
}
export default WeatherData;
