import React, { Component } from 'react';
import PropTypes from 'prop-types';

class WeatherExtraInfo extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const { humidity, wind } = this.props;
        // template string -> `` 
        // { `${humidity} % -`}

        return (
            <div className="weather-extra-info-content">
                <h5 className="extra-info-title">Detalles</h5>
                <p className="extre-info-text">{ `Humedad: ${humidity} %`}</p>
                <p className="extre-info-text">{ `Vientos: ${wind}`}</p>
            </div>
        );
    }
}

/**
 * Definimos el tipo de dato que debe llegar para ser usado en el componente
 */
WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
};

export default WeatherExtraInfo;