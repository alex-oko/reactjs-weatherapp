import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Location extends Component {
    constructor(props){
        super(props);
    }

    render() {
        //Destructurando Obj
        const { city } = this.props;
        // Sin destructurar
        //const city = this.props.city;
        return (
            <div>
                <h2>{city}</h2>
            </div>
        )
    }
}

/**
 * Definimos el tipo de dato que debe llegar para ser usado en el componente
 */
Location.propTypes = {
  city: PropTypes.string.isRequired  
};

export default Location;
