import React from 'react';
import Location from './Location';
import WheaterData from './WeatherData';

const WeatherLocation = () => (
    <div>
        <Location city={"Jmaundi"}/>
        <WheaterData/>
    </div>
);

export default WeatherLocation;